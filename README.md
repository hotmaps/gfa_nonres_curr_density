# Heated gross floor area density map of non-residential buildings in EU28 + Switzerland, Norway and Iceland for the year 2015


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the non-residential gross floor area in EU28 on hectare (ha) level. 

For the estimated gross floor area of buildings on the hectare level, we use two independent approaches. The first approach builds on the population grid on the hectare level and derives the estimated gross floor area using the average gross floor area per dwelling and the average persons  per  household;  this  data  is  available  from  the  building  census  for  most  European  NUTS3 regions. While this approach is quite reasonable for the residential building stock, its prediction quality  is quite  poor for  areas with  a  high  share of non‐residential buildings.  Therefore, we build a second independent layer of the gross floor area. 
 
For the second approach to derive the gross floor areas we use the data from the European Settlement Map and data from the building layer of the OpenStreetMap (OSM) database [1]. For the European Settlement layer [2] we estimate the gross floor area from the share of the plot area that is counted as sealed by buildings and a building height model considering the  average share  of  sealed  area.  For  the  estimated  gross  floor  area  derived  from  the  OpenStreetMap (OSM) [1] database, we evaluate the share of covered plot area per hectare level for all buildings covered by this data source. The gross floor area is then calculated from a generic building height model (Figure 9), accompanied by the average regional floor height derived from those buildings (~6 Mil. buildings spread over Europe) in the local neighbourhood, for which information on the number of floors is stored in the OSM database. 

In a next step, the so derived gross floor areas are compared against that from the population based approach. If the OSM‐based approach derives lower areas, the areas are scaled (up to a factor of  4) accordingly.  If the OSM‐based approach results in  an average  floor area  per  inhabitant  in  a  grid  cell  of  less  than  15  m²,  then  a  quality  indicator, which defines the completeness of the OSM data (see Figure 10), is reduced and the weight of the population‐based approach that given grid cell is then reduced subsequently.

The heated gross floor area will be used in the Hotmaps toolbox to estimate the heating demand in service sector.

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3 page 37.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.
For the residential heated gross floor area, statistical data are available for most countries on the level of NUTS3. Again, manually performed data quality checks indicate that results are plausible on the hectare level of most regions. However, as of now, we do not factor in the fact, that the heat area per inhabitant often decrease with an increasing population density. For  NUTS3  regions  with  a  strong  urban  versus  rural  area  gradient,  this  might  lead  to  overestimation of the heated residential gross floor area in urban areas. Regarding the heated gross floor area of non‐residential buildings, data sources are even uncertain on the NUTS0 level. Data quality checks indicate that the sum of residential and non‐ residential heated gross floor area are plausible as well as the ratio between residential and non‐residential gross floor area,  even  though  the  later  indicator  might  not  hold  for  grid  cells which contain only  few  buildings.


### References
[1] [Open Street Map (OSM)](www.openstreetmap.org)

[2] [European Settlement Map](https://land.copernicus.eu/pan-european/GHSL/european-settlement-map/esm-2012-release-2017-urban-green/view)


## How to cite
Simon Pezzutto, Stefano Zambotti, Silvia Croce, Pietro Zambelli, Giulia Garegnani, Chiara Scaramuzzino, Ramón Pascual Pascuas, Alyona Zubaryeva, Franziska Haas, Dagmar Exner (EURAC), Andreas Mueller (e-think), Michael Hartner (TUW), Tobias Fleiter, Anna-Lena Klingler, Matthias Ku¨hnbach, Pia Manz, Simon Marwitz, Matthias Rehfeldt, Jan Steinbach, Eftim Popovski (Fraunhofer ISI) Reviewed by Lukas Kranzl, Sara Fritz (TUW)
Hotmaps Project, D2.3 WP2 Report – Open Data Set for the EU28, 2018 [www.hotmaps-project.eu](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) 


## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2020: Andreas Mueller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

